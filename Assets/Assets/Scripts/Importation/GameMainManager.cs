﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMainManager : MonoBehaviour {
	public GUIManager GUI;

	public Transform EmptyWorld;
	public Transform ConciergerieWorld;
	public Transform PlageWorld;

	public Transform ImportedObjectsPrefab;

	private Transform currentWorld;

	private bool isDisplayed; //Used to determine paused state

	void Start ()
	{
		EmptyWorld.gameObject.SetActive (true);
		ConciergerieWorld.gameObject.SetActive (false);
		PlageWorld.gameObject.SetActive (false);

		currentWorld = EmptyWorld;
	}

	private void hideCurrentWorld(){
		currentWorld.gameObject.SetActive(false);
	}

	public void LoadConciergerie() {
		hideCurrentWorld ();
		ConciergerieWorld.gameObject.SetActive (true);
		currentWorld = ConciergerieWorld;
	}

	public void LoadPlageWorld() {
		hideCurrentWorld ();
		PlageWorld.gameObject.SetActive (true);
		currentWorld = PlageWorld;
	}

	public void ResetWorld(Transform world){
		Destroy (world.GetChild(0));
		Transform NewWorld = Instantiate (ImportedObjectsPrefab) as Transform;
	}

	public Transform GetCurrentWorld(){
		return currentWorld;
	}
}
