﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImportationManager : MonoBehaviour {
	public GUIManager GUI;
	public GameMainManager Game;

	public void ImportGameObject(GameObject objectToInstantiate){
		/* Importation d'un objet dans l'environnement */

		GameObject createdObject = Instantiate(objectToInstantiate) as GameObject; //Instantiation à partir de la prefab 
		createdObject.transform.SetParent(Game.GetCurrentWorld());
		//Debug.Log(Game.GetCurrentWorld());
		CreateRigidBody(createdObject);

		if (!createdObject.GetComponent<Collider>()){
			CreateBoxCollider(createdObject); //Creation d'un box Collider adapté
		} 

		if (!createdObject.GetComponent<Rigidbody>()) {
			CreateRigidBody (createdObject);
		}

		SetObjectPosition(createdObject); //Positionnement de l'objet devant la caméra

		GUI.UnPause();
	}    

	private void SetObjectPosition(GameObject currentObject){
		/*Positionne l'objet devant la caméra, et orienté vers la camera*/

		float distance = 0.8f; //Distance à la camera
		Vector3 position = Camera.main.transform.position +  Camera.main.transform.forward * distance; //Position de l'objet devant la caméra
		Quaternion rotation = Camera.main.transform.rotation * Quaternion.Euler(0,180f,0); //Orientation de l'objet vers la caméra

		currentObject.transform.position = position; //Mise à jour de la position
		//currentObject.transform.rotation = rotation; //Mise à jour de l'orientation
	}

	private void CreateBoxCollider(GameObject currentObject){
		/*Créer un box collider adapté à a taille du gameObject en enveloppant tous les renderers enfants (enveloppe minimale) */

		BoxCollider collider = currentObject.AddComponent<BoxCollider>(); //Création du collider
		Bounds bounds = new Bounds(Vector3.zero, Vector3.zero); //Création d'un cube de taille 0, à l'origine
		Renderer[] childrenRenderers = currentObject.GetComponentsInChildren<Renderer>(); //Liste des renderers enfant

		foreach(Renderer renderer in childrenRenderers) {
			bounds.Encapsulate(renderer.bounds); //Agrandit le cube "bounds" pour contenir chaque renderer enfant
		}

		collider.size = bounds.size; //Mise à jour du collider
		collider.center = bounds.center - currentObject.transform.position; //Positionne le center du collider pour s'adapter à la position de l'objet
	}

	private void CreateRigidBody(GameObject currentObject){
		Rigidbody rigidBody = currentObject.AddComponent<Rigidbody>(); //Ajout d'un RigidBody
		//rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		//currentObject.AddComponent<VelocityLimitation> ();
	}

	private void CreateRenderer(GameObject currentObject){
		Renderer renderer = currentObject.AddComponent<Renderer> ();
	}
}
