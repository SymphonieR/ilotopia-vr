﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityLimitation : MonoBehaviour {
	
       public float maxSpeed = 1.5f;//Replace with your max speed
       void FixedUpdate()
       {
             if(this.gameObject.GetComponent<Rigidbody>().velocity.magnitude > maxSpeed)
             {
                    this.gameObject.GetComponent<Rigidbody>().velocity = this.gameObject.GetComponent<Rigidbody>().velocity.normalized * maxSpeed;
             }
       }
}
