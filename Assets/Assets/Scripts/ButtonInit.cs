﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInit : MonoBehaviour {
	public GameObject GM;
	public RectTransform panel;
	public Button button;
	public List<GameObject> createdObjects = new List<GameObject>();

	// Use this for initialization
	void Start () {
		Object[] prefabs = Resources.LoadAll("Prefabs", typeof(GameObject));
		int objectsPerLines = 7;
		panel.GetComponent<RectTransform>().sizeDelta = new Vector2(0,-420+((prefabs.Length-1)%objectsPerLines)*150);

		for (int i=0; i<prefabs.Length; i++){
			GameObject currentObject = (GameObject)prefabs[i];

			Button createdButton = Instantiate(button) as Button;
			createdButton.onClick.AddListener(() => CreateObject(currentObject.name));

			createdButton.transform.SetParent(panel.transform, true);
			createdButton.GetComponentInChildren<Text>().text = currentObject.name;
			createdButton.transform.localPosition = new Vector3 (-390+(i%objectsPerLines)*125, -50-103*(i/objectsPerLines), 0);
		}
	}

	private void SetPosition(GameObject gameObject){
		    float distance = 4;
		    Vector3 position = Camera.main.transform.position +  Camera.main.transform.forward * distance;
		    Quaternion rotation = Camera.main.transform.rotation * Quaternion.Euler(0,180f,0);

		    gameObject.transform.position = position;
		    gameObject.transform.rotation = rotation;
	}
	
 	private void CreateBoxCollider(GameObject gameObject){
 		//Créer un box collider adapté à a taille du gameObject en enveloppant tous les renderers enfants (enveloppe minimale) 
 		BoxCollider collider = gameObject.AddComponent<BoxCollider>();
	    Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
	    Renderer[] childrenRenderers = gameObject.GetComponentsInChildren<Renderer>();
	     
   		foreach(Renderer renderer in childrenRenderers) {
        	bounds.Encapsulate(renderer.bounds);
     	}

     	collider.size = bounds.size;
		collider.center = bounds.center - gameObject.transform.position;
 	}

	public void CreateObject(string prefabName){
	    // Instantie l'objet
	    GameObject gameObject = Instantiate(Resources.Load("Prefabs/" + prefabName, typeof(GameObject))) as GameObject;

	    if (!gameObject.GetComponent<Collider>()){
	    	CreateBoxCollider(gameObject);
	    } 

	   	SetPosition(gameObject);
	    gameObject.AddComponent<Rigidbody>();
	    gameObject.AddComponent<Delete>();

	    GM.GetComponent<Manager>().UnPause();
	}    
}
