﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

// rename this class to suit your needs
public class TestInstantiation : MonoBehaviour
{
	public GameObject prefab;

	// list that holds all created objects - deleate all instances if desired
	public List<GameObject> createdObjects = new List<GameObject>();

	private void SetPosition(GameObject gameObject){
		    float distance = 4;
		    Vector3 position = Camera.main.transform.position +  Camera.main.transform.forward * distance;
		    Quaternion rotation = Camera.main.transform.rotation * Quaternion.Euler(0,180f,0);

		    gameObject.transform.position = position;
		    gameObject.transform.rotation = rotation;
	}
	
 	private void CreateBoxCollider(GameObject gameObject){
 		//Créer un box collider adapté à a taille du gameObject en enveloppant tous les renderers enfants (enveloppe minimale) 
 		BoxCollider collider = gameObject.AddComponent<BoxCollider>();
	    Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
	    Renderer[] childrenRenderers = gameObject.GetComponentsInChildren<Renderer>();
	     
   		foreach(Renderer renderer in childrenRenderers) {
        	bounds.Encapsulate(renderer.bounds);
     	}

     	collider.size = bounds.size;
		collider.center = bounds.center - gameObject.transform.position;
 	}

	public void CreateObject()
	{
 		if (prefab != null)
	 	{
		    // Instantie l'objet
		    GameObject gameObject = (GameObject)Instantiate(prefab);

		    if (!gameObject.GetComponent<Collider>()){
		    	CreateBoxCollider(gameObject);
		    } 

		   	SetPosition(gameObject);
		    gameObject.AddComponent<Rigidbody>();
		    gameObject.AddComponent<Delete>();

		    createdObjects.Add(gameObject);
	 	}
	}    
}