﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimeManager : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		GetComponent<Text>().text = System.DateTime.Now.ToString("HH:mm");
	}
}
