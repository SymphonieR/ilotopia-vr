﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ControllerManager : MonoBehaviour {

	public GUIManager GUI;
	private SteamVR_TrackedObject trackedObj;
	private SteamVR_LaserPointer laserPointer;
	//private float nextUpdate = 0.0f;
	//private float interval = 0.5f;
	
	private SteamVR_Controller.Device Controller
	{
	    get { return SteamVR_Controller.Input((int)trackedObj.index); }
	}

	void Awake()
	{
	    trackedObj = GetComponent<SteamVR_TrackedObject>();
		laserPointer = GetComponent<SteamVR_LaserPointer>();
	}

	void Start()
	{
		if (GUI.EditMode ()) {
			laserPointer.Show ();
		} else {
			laserPointer.Hide ();
		}

		InvokeRepeating ("HoldButton", 2.0f, 0.3f);
	}

	// Update is called once per frame
	void Update () {
		if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
		{
			if (!GUI.IsActive()){
				GUI.Pause();
				laserPointer.Show ();
			} else {
				GUI.UnPause();
				if (!GUI.EditMode ()) {
					laserPointer.Hide ();
				}
			}
		}
		if (Controller.GetPressDown (SteamVR_Controller.ButtonMask.Touchpad)) {
			if ((Controller.GetAxis().x > 0.5f || Controller.GetAxis ().x < -0.5f) && GUI.MainPanelIsActive()) {
				GUI.SwitchMenuPanel ();
			}
		}

		/*ClickButton ();
		if (Time.time >= nextUpdate) {
			nextUpdate += interval;
		}*/
	}

	void HoldButton() {
		if (Controller.GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
			Debug.Log ("ok");
			GameObject currentObject = EventSystem.current.currentSelectedGameObject;
			//Debug.Log (currentObject);
			if (currentObject != null && currentObject.GetComponent<Button> () != null) {
				
				ExecuteEvents.Execute (currentObject, new PointerEventData (EventSystem.current), ExecuteEvents.submitHandler);
			}
		}
	}
}
