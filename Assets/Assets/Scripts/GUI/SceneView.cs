﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class SceneView : MonoBehaviour {
	public Button SceneButtonPrefab;

	public SaveManager Manager;
	private string[] scenes;

	void Awake () {
		scenes = Directory.GetFiles(Application.persistentDataPath, "*.save", SearchOption.TopDirectoryOnly);
		InitializeScenePanel ();
	}

	public void UpdateScenePanel(){
		Manager.InitializeSceneButtons ();
		scenes = Directory.GetFiles(Application.persistentDataPath, "*.save", SearchOption.TopDirectoryOnly);
		InitializeScenePanel ();
	}

	public void InitializeScenePanel(){
		for (int i = 0; i < scenes.Length; i++) {
			string scene = Path.GetFileNameWithoutExtension (scenes [i]);
			Button sceneButton = CreateSceneButton (scene, i);
			Manager.AddSceneButton (sceneButton);
		}
	}
		
	public Button CreateSceneButton (string sceneName, int i) {
	/* Création d'une liste de boutons correspondants aux objets à importer dans la catégorie désigné par "categoryName"
		- string categoryName : nom du dossier contenant les modèles à importer (si categoryName = "", alors tous les objets du dossiers Prefabs sont importés)
		- Transform categoryPanel : ScrollView associé à la catégorie
	*/
	
		//Coordonnées pour l'instantiation des boutons
		int objectsPerLines = 3; 
		int xStart = -200; 
		int yStart = 80;
		int xDistance = 200;
		int yDistance = 140;

		Button sceneButton = Instantiate(SceneButtonPrefab, gameObject.transform.GetChild(0).GetChild(0)) as Button; //Instantion du bouton associé à la prefab
		Texture2D tex = LoadImage (sceneName);
		sceneButton.transform.GetChild(1).GetComponent<Image>().sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));

		sceneButton.onClick.AddListener(() => Manager.ActiveSceneButton(i)); //Ajout d'un Listener pour l'importation de l'objet à partir de la prefab currentObject

		sceneButton.GetComponent<RectTransform>().anchoredPosition = new Vector3 (xStart+(i%objectsPerLines)*xDistance, yStart-(i/objectsPerLines)*yDistance, 0); //Mise en position du bouton
		sceneButton.GetComponentInChildren<Text>().text = sceneName; //Modification du nom du bouton

		return sceneButton;
	}

	public Texture2D LoadImage(string sceneName){
		WWW www = new WWW (Application.persistentDataPath + "/" + sceneName + ".png");
		Texture2D tex = www.texture;
		return tex;
	}
}
