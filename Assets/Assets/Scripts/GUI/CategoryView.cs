﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CategoryView : MonoBehaviour {
	public ImportationManager Importation;
	public GUIManager GUI;
	public GameMainManager Game;

	private Transform CategoriesMenuPanel; //Panel contenant le menu des catégories

	public Button CategoryButtonPrefab; //Prefab associé aux boutons du menu "catégories"
	public Transform CategoryPanelPrefab; //Prefab associé au ScrollView de chaque catégorie
	public Button ImportButtonPrefab; //Prefab associé aux boutons pour l'importation des objets

	void Awake() {
		CategoriesMenuPanel = gameObject.transform.GetChild (2); 

		/* Initialise la fenetre d'importation d'objets sous forme de categories
				Pour chaque catégorie de la liste, on crée un ScrollView, contenant la liste des objets à importer, 
				et un bouton d'accès pour accéder à ce ScrollView, dans le menu "categories". 
				Au démarrage, seule la catégorie "Tous" est active.
		*/

		// Initialisation de la categorie "Tous" avec tous les objets
		InitializeCategoryPanel("Tous", 0, true);

		// Liste des chemins des dossiers (catégories) contenant les objets à importer
		string[] categoriesPath = Directory.GetFiles(Application.dataPath + "/Assets/Resources/Prefabs", "*", SearchOption.TopDirectoryOnly);

		for (int i=0; i<categoriesPath.Length; i++){
			string categoryName = Path.GetFileNameWithoutExtension (categoriesPath [i]); //Nom de la catégorie, sans l'extension et sans le chemin
			InitializeCategoryPanel(categoryName, i+1, false);
		}
	}

	public void InitializeCategoryPanel(string categoryName, int index, bool enable) {
		Transform categoryPanel = CreateCategoryPanel (categoryName, enable); //Création du ScrollView
		CreateImportButton (categoryPanel, categoryName); // Création des boutons à l'intérieur de la ScrollView
		CreateCategoryButton (categoryName, index); //Création du bouton d'accès à la ScrollView d'indice i+1
		GUI.AddCategoryPanel (categoryPanel); // Ajout du ScrollView de la catégorie courantes à la liste de tous les ScrollView
	}

	public void CreateCategoryButton(string category, int i){
		/* Création du bouton associé à la catégorie, permettant d'accèder à la ScrollView de la catégorie 
			- string category : nom de la catégorie
			- int i : indice de la catégorie
		*/

		Button categoryButton = Instantiate (CategoryButtonPrefab) as Button; //Instantiation à partir de la prefab

		categoryButton.transform.SetParent (CategoriesMenuPanel.transform, false); //Ajout du bouton au menu "categories"
		categoryButton.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-90.0f, -17.5f - i* 37.0f, 0.0f); //Placement du bouton en dessous des boutons précédents
		categoryButton.GetComponentInChildren<Text> ().text = category; //Affichage du nom de la catégorie associée sur le bouton

		categoryButton.onClick.AddListener(() => GUI.SwitchCategoryPanel(i)); //Listener pour changer la catégorie active
	}

	public Transform CreateCategoryPanel(string category, bool enable){
		/* Création du ScrollView associé à la catégorie, contenant les boutons associés aux objets à importer de la catégorie
			- string category : nom de la category
			- categoryPanel : ScrollView associé à la catégorie
		*/

		Transform categoryPanel = Instantiate (CategoryPanelPrefab) as Transform; //Instantiation du ScrollView

		categoryPanel.transform.SetParent (gameObject.transform, false); //Ajout du ScrollView à la fenêtre "objets"
		categoryPanel.name = category; //Changement du nom du ScrollView
		categoryPanel.gameObject.SetActive (enable); //Désactiver le ScrollView

		return categoryPanel;
	}

	public string GetCategoryPath(string categoryName){
		/* Retourne le chemin du dossier catégorie sous la forme d'un string 
			- Si la catégorie est "Tous", le dossier est "Prefabs"
			- Sinon la catégorie est "Prefabs/Catégorie"
		*/
		return (categoryName == "Tous") ? "Prefabs" : "Prefabs/" + categoryName; 
	}

	public void CreateImportButton (Transform categoryPanel, string categoryName) {
		/* Création d'une liste de boutons correspondants aux objets à importer dans la catégorie désigné par "categoryName"
			- string categoryName : nom du dossier contenant les modèles à importer (si categoryName = "", alors tous les objets du dossiers Prefabs sont importés)
			- Transform categoryPanel : ScrollView associé à la catégorie
		*/

		string categoryPath = GetCategoryPath (categoryName);
		Object[] prefabs = Resources.LoadAll(categoryPath, typeof(GameObject)); //Liste de tous les prefabs contenues dans le dossier categoryName

		//Coordonnées pour l'instantiation des boutons
		int objectsPerLines = 4; 

		float xStart = 0;
		float yStart = 0;

		float xDistance = 120f;
		float yDistance = 105f;

		float height = yDistance * ((prefabs.Length  / objectsPerLines) + 1);
		float width = objectsPerLines * xDistance;

		RectTransform rt = categoryPanel.GetChild (0).GetChild (0).GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (0, height);

		for (int i=0; i<prefabs.Length; i++){
			GameObject currentObject = (GameObject)prefabs[i]; //Acquisition de la prefab d'indice i

			Button importButton = Instantiate(ImportButtonPrefab) as Button; //Instantion du bouton associé à la prefab
			importButton.onClick.AddListener(() => Importation.ImportGameObject(currentObject)); //Ajout d'un Listener pour l'importation de l'objet à partir de la prefab currentObject

			importButton.GetComponent<RectTransform>().anchoredPosition = new Vector3 (xStart+(i%objectsPerLines)*xDistance, yStart-(i/objectsPerLines)*yDistance, 0); //Mise en position du bouton
			importButton.transform.SetParent(categoryPanel.transform.GetChild(0).GetChild(0), false); //Ajout du bouton créé dans le ScrollView


			importButton.GetComponentInChildren<Text>().text = currentObject.name; //Modification du nom du bouton
			Texture2D tex = LoadImage (currentObject.name);
			importButton.transform.GetChild(1).GetComponent<Image>().sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		}
	}

	public Texture2D LoadImage(string objectName){
		WWW www = new WWW (Application.dataPath + "/Assets/Resources/Icons/" + objectName +".jpg");
		Texture2D tex = www.texture;
		return tex;
	}
}
