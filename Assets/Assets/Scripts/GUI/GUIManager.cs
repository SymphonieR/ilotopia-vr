﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class GUIManager : MonoBehaviour {
	public Transform Canvas;
	public Toggle Edit;

	private Transform editionPanel;
	private Transform demoPanel;

	private List<Transform> panels = new List<Transform> ();
	private const int DISABLED = 22;
	private int currentPanel;

	private List<Transform> menuPanels = new List<Transform>();
	private int currentMenu;

	private List<Transform> categorymenuPanels = new List<Transform>(); //Liste de tous les ScrollViews associés aux catégories
	private int currentCategory; //Indice de la catégorie active 

	void Awake () {
		Transform mainPanel = Canvas.GetChild (0);
		Transform gamepadPanel = Canvas.GetChild (1);
		Transform settingsPanel = Canvas.GetChild (2);

		panels.Add (mainPanel);
		panels.Add (gamepadPanel);
		panels.Add (settingsPanel);

		editionPanel = mainPanel.GetChild (0);
		demoPanel = mainPanel.GetChild (1);

		Transform importPanel = editionPanel.GetChild (2);
		Transform scenesPanel = editionPanel.GetChild (3);

		menuPanels.Add (importPanel);
		menuPanels.Add (scenesPanel);

		currentMenu = 0;
		currentCategory = 0; //Au démarrage la catégorie "Tous" est active

	}

	void Start () {
		HideAllPanelWrapper ();
		if (EditMode ()) {
			ShowEditPanel ();
		} else {
			ShowDemoPanel ();
		}
		ShowMenuPanel (0); 
		//ShowCategoryPanel (0);
		HideMenuPanel (1);
	}

	public bool EditMode(){
		return Edit.isOn;
	}

	public void Pause () {
		SetCanvasPosition ();
		ShowPanelWrapper (0);
		Time.timeScale = 0.0f;
	}

	public void UnPause() {
		HidePanelWrapper (currentPanel);
		Time.timeScale = 1.0f;
	}

	public bool IsActive () {
		return (currentPanel != DISABLED) ? true : false;
	}

	public bool MainPanelIsActive () {
		return (currentPanel == 0) ? true : false;
	}

	public void ShowDemoPanel(){
		editionPanel.gameObject.SetActive (false);
		demoPanel.gameObject.SetActive (true);
	}

	public void ShowEditPanel(){
		demoPanel.gameObject.SetActive (false);
		editionPanel.gameObject.SetActive (true);
	}

	public void ShowPanelWrapper (int i) {
		HideCurrentPanelWrapper ();
		panels [i].gameObject.SetActive (true);
		currentPanel = i;
	}

	public void HidePanelWrapper(int i) {
		panels [i].gameObject.SetActive (false);
		currentPanel = DISABLED;
	}

	public void HideCurrentPanelWrapper() {
		if (currentPanel != DISABLED) {
			panels [currentPanel].gameObject.SetActive (false);
		}
		currentPanel = DISABLED;
	}

	public void HideAllPanelWrapper () {
		foreach (Transform panel in panels) {
			panel.gameObject.SetActive (false);
		}
		currentPanel = DISABLED;
	}

	public void ShowMainPanel(){
		ShowPanelWrapper(0);
	}

	public void ShowHelpPanel(){
		ShowPanelWrapper (1);
	}

	public void ShowSettingsPanel(){
		ShowPanelWrapper(2);		
	}
		
	private void SetCanvasPosition(){
		float distance = 0.8f;
		Vector3 position = Camera.main.transform.position +  Camera.main.transform.forward * distance;

		Canvas.transform.position = new Vector3(position[0], Camera.main.transform.position.y - 0.2f, position[2]);
		Canvas.transform.Rotate(0,Camera.main.transform.eulerAngles.y - Canvas.transform.eulerAngles.y,0);
	}

	public void HideMenuPanel(int i) {
		menuPanels [i].gameObject.SetActive (false);
	}

	public void ShowMenuPanel(int i) {
		menuPanels [i].gameObject.SetActive (true);
	}

	public void SwitchMenuPanel() {
		HideMenuPanel (currentMenu);
		currentMenu = currentMenu == 0 ? 1 : 0;
		ShowMenuPanel (currentMenu);
	}
		
	public void AddCategoryPanel(Transform CategoryPanel) {
		categorymenuPanels.Add (CategoryPanel);
	}

	public void HideCategoryPanel(int i) {
		categorymenuPanels [i].gameObject.SetActive (false);
	}

	public void ShowCategoryPanel(int i) {
		categorymenuPanels [i].gameObject.SetActive (true);
	}
		
	public void SwitchCategoryPanel(int i) {
		/* Désactive la catégorie courante et active la catégorie d'indice i*/

		HideCategoryPanel (currentCategory); //Desactive la catégorie courante
		currentCategory = i; //Met à jour l'indice de la catégorie active
		ShowCategoryPanel (currentCategory); //Active la catégorie d'indice i
	}
}
