﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroll : MonoBehaviour {
	// Update is called once per frame
	float shift = 0.12f;
	public void ScrollContentUp () {
		transform.position = new Vector3(transform.position.x, transform.position.y - shift, transform.position.z);
	}

	public void ScrollContentDown () {
		transform.position = new Vector3(transform.position.x, transform.position.y + shift, transform.position.z);
	}
}
