﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SavedObject {
	public string path;

	public float x;
	public float y;
	public float z;

	public float xr;
	public float yr;
	public float zr;
	public float wr;

	public float scaleX;
	public float scaleY;
	public float scaleZ;

	public SavedObject(){
		path = "";

		x = 0;
		y = 0;
		z = 0;

		scaleX = 1;
		scaleY = 1;
		scaleZ = 1;
	}

	public SavedObject(GameObject currentObject){
		path = currentObject.transform.name;

		x = currentObject.transform.position.x;
		y = currentObject.transform.position.y;
		z = currentObject.transform.position.z;

		xr = currentObject.transform.rotation.x;
		yr = currentObject.transform.rotation.y;
		zr = currentObject.transform.rotation.z;
		wr = currentObject.transform.rotation.w;

		scaleX = currentObject.transform.localScale.x;
		scaleY = currentObject.transform.localScale.y;
		scaleZ = currentObject.transform.localScale.z;
	}

	public Vector3 GetPosition(){
		return new Vector3 (x,y,z);
	}

	public Quaternion GetRotation(){
		return new Quaternion(xr,yr,zr,wr);
	} 

	public Vector3 GetScale(){
		return new Vector3 (scaleX, scaleY, scaleZ);
	}
}
