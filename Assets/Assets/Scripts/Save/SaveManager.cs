﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SaveManager : MonoBehaviour {
	public SceneView View;
	public GUIManager GUI;

	public Transform World;
	public Transform ScenePrefab;
	public Transform SceneMenu;

	private Transform scene;
	private string currentScene = "";

	private List<Button> sceneButtons = new List<Button> ();
	private int currentButtonIndex = 0;

	void Awake () {
		scene = World.GetChild (2);
		SceneMenu.gameObject.SetActive (false);
	}

	public void InitializeSceneButtons() {
		foreach (Button sceneButton in sceneButtons) {
			Destroy (sceneButton.gameObject);
		}
		sceneButtons = new List<Button> ();
	}

	public void AddSceneButton(Button sceneButton){
		sceneButtons.Add (sceneButton);
	}

	public void ActiveSceneButton(int i){
		InactiveSceneButton ();
		currentButtonIndex = i;
		SceneMenu.gameObject.SetActive (true);
		sceneButtons [i].transform.GetChild (2).gameObject.SetActive (true);
	}

	public void InactiveSceneButton(){
		if (currentButtonIndex < sceneButtons.Count)
			sceneButtons [currentButtonIndex].transform.GetChild (2).gameObject.SetActive (false);
		
		currentButtonIndex = 0;
		SceneMenu.gameObject.SetActive (false);
	}

	IEnumerator Wait() {
		yield return new WaitForSeconds (0.1f);
		View.UpdateScenePanel ();
		GUI.Pause ();
	}

	public void NewScene() {
		CreateNewScene ();
		currentScene = "";

		GUI.UnPause ();
		Debug.Log ("New Scene");
	}
		
	public void SaveScene() {
		string fileName = currentScene;

		Save save = CreateSave ();
		BinaryFormatter bf = new BinaryFormatter ();

		if (fileName == "") {
			Debug.Log ("create");
			fileName = System.Guid.NewGuid ().ToString ();
		}

		FileStream file = File.Create (Application.persistentDataPath + "/" + fileName + ".save");

		bf.Serialize(file, save);
		file.Close ();

		GUI.UnPause ();

		ScreenCapture.CaptureScreenshot (Application.persistentDataPath + "/" + fileName + ".png");
		StartCoroutine (Wait());

		currentScene = fileName;

		Debug.Log ("Save Complete");
	}
		
	public void LoadScene(){
		string sceneName = sceneButtons [currentButtonIndex].GetComponentInChildren<Text>().text;
		string filePath = Application.persistentDataPath + "/" + sceneName + ".save";

		if (File.Exists (filePath)) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (filePath, FileMode.Open);
			Save save = (Save)bf.Deserialize (file);
			file.Close ();

			CreateNewScene ();

			foreach (SavedObject savedObject in save.objects) {
				GameObject importedObject = Instantiate (Resources.Load("Prefabs/" + savedObject.path), scene) as GameObject;
				importedObject.name = savedObject.path;
				importedObject.transform.position = savedObject.GetPosition ();
				importedObject.transform.rotation = savedObject.GetRotation ();
				importedObject.transform.localScale = savedObject.GetScale ();
			}

			currentScene = sceneName;
			Debug.Log ("Load Complete");

			GUI.UnPause ();
		} else {
			Debug.Log ("No Save");
		}
	}
			
	public void DeleteSave () {
		string sceneName = sceneButtons [currentButtonIndex].GetComponentInChildren<Text>().text;
		string filePath = Application.persistentDataPath + "/" + sceneName + ".save";

		if (File.Exists (filePath)) {
			File.Delete (filePath);
			InactiveSceneButton (); 
			View.UpdateScenePanel ();
			Debug.Log ("Delete Complete");
		} else {
			Debug.Log ("No Save");
		}
	}

	public Save CreateSave() {
		Save save = new Save ();

		for(int i=0; i<scene.childCount; i++) {
			GameObject currentObject = scene.GetChild (i).gameObject;
			SavedObject importedObject = new SavedObject (currentObject);
			save.AddImportedObject (importedObject);
		}

		return save;
	}

	public void CreateNewScene() {
		Destroy (scene.gameObject);
		scene = Instantiate (ScenePrefab, World) as Transform;
	}
}