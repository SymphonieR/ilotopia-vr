﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save {
	public List<SavedObject> objects = new List<SavedObject>();

	public void AddImportedObject(SavedObject importedObject){
		objects.Add (importedObject);
	}
}
