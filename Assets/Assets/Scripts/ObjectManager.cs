﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ObjectManager : MonoBehaviour {
	
	//public GameObject cube;
	public Slider resizeSlider;
	public Slider rotationSlider;
	public GameObject boutonModifierTaille;

	private float sizeIncrement = 0.1f;

	public Transform canvas;
	public Transform UIPanel; //Will assign our panel to this variable so we can enable/disable it
	public Transform PanelDimension;
	public Transform PanelRotation;
	bool isOpened; //Used to determine paused state

	RaycastHit hit;
	Ray ray;
	GameObject target;
	Vector3 initialScale;

	void Start ()
	{
		
		isOpened = false;//make sure isPaused is always false when our scene opens
		UIPanel.gameObject.SetActive(false); //make sure our pause menu is disabled when scene starts
		PanelDimension.gameObject.SetActive(false);
		PanelRotation.gameObject.SetActive(false);
	}
		
	public void SetTarget(GameObject objectTarget){
		target = objectTarget;
		Debug.Log (target);
	}

	public bool Target() {
		return (target); 
	}

	public void Show()
	{	
		isOpened = true;
		setPositionPanel();
		UIPanel.gameObject.SetActive(true); //turn on the pause menu
		Time.timeScale = 0f; //pause the game

	}

	public void Hide()
	{	
		isOpened = false;
		UIPanel.gameObject.SetActive(false); //turn off pause menu
		Time.timeScale = 1f; //resume game

	}

	public bool IsOpened(){
		return isOpened;
	}

	private void setPositionPanel(){

		float height = target.GetComponent<Collider> ().bounds.size.y;

		Vector3 position = new Vector3 (target.transform.position.x, target.transform.position.y + height, target.transform.position.z);

		canvas.transform.position = position;
		canvas.transform.eulerAngles = new Vector3(0,Camera.main.transform.eulerAngles.y,0);
	}
		
	/*public void modifierCouleur() {
		// Find a random color

		float red = Random.Range(0f,1f);
		float green = Random.Range(0f,1f);
		float blue = Random.Range(0f,1f);
		Color newColor = new Color( red, green, blue );

		// Apply color
		cube.GetComponent<MeshRenderer>().material.color = newColor;

		//Activer le bouton ModifierCouleur et désactiver le slider 
		if (mSlider.gameObject.activeSelf) {
			mSlider.gameObject.SetActive (false);
			boutonModifierTaille.SetActive (true);
		}

	}*/

	// ---------- Menu principal ------------
	public void ShowResize(){
		UIPanel.gameObject.SetActive(false);
		PanelDimension.gameObject.SetActive(true);
		resizeSlider.value = target.transform.localScale.x;
	}

	public void ShowRotate(){
		//rotationSlider.value = target.transform.eulerAngles.y;
		UIPanel.gameObject.SetActive(false);
		PanelRotation.gameObject.SetActive(true);
	}

	public void Delete(){
		Destroy(target); 
		target = null;
		Hide ();
	}

	public void QuitMenu(){
		if (isOpened) {
			Hide();
		}
	}

	// ---------- Menu Dimensionnement ------------
	/*public void Resize(){
		target.transform.localScale = (resizeSlider.value)*(Vector3.one);
	}*/
	void setTargetScale(float scale){
		target.transform.localScale = scale * Vector3.one;
	}

	public void IncreaseSize(){
		if (target.transform.localScale.x < 2.0f) {
			float scale = target.transform.localScale.x + sizeIncrement;
			setTargetScale (scale);
		}
	}

	public void ReduceSize(){
		if (target.transform.localScale.x > 0.5f) {
			float scale = target.transform.localScale.x + sizeIncrement;
			setTargetScale (scale);
		}
	}

	public void QuitResize(){
		UIPanel.gameObject.SetActive(true);
		PanelDimension.gameObject.SetActive(false);
	}
		
	// ---------- Menu Rotation ------------
	/*public void Rotate(){
		Vector3 tourner = (-rotationSlider.value) * (Vector3.up);
		target.transform.rotation = Quaternion.Euler(0, rotationSlider.value, 0);
	}*/

	void setTargetRotation(float rotation){
		target.transform.rotation = Quaternion.Euler (0, rotation, 0);
	}

	public void TurnRight() {
		if (target.transform.rotation.eulerAngles.y > 180.0f) {
			float rotation = target.transform.rotation.eulerAngles.y + 1.0f;
			setTargetRotation (rotation);
		}
	}

	public void TurnLeft() {
		if (target.transform.rotation.eulerAngles.y < -180.0f) {
			float rotation = target.transform.rotation.eulerAngles.y - 1.0f;
			setTargetRotation (rotation);
		}
	}

	public void QuitRotation(){
		UIPanel.gameObject.SetActive(true);
		PanelRotation.gameObject.SetActive(false);
	}
}