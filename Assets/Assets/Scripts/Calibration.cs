﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calibration : MonoBehaviour
{

    public GameObject firstCube, secondCube, ControllerCube, guitoune;
    public SteamVR_TrackedObject trackedObj;
    private float x1, z1, x2, z2;
    private int point;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Use this for initialization
    void Start()
    {
        x1 = 0;
        x2 = 0;
        z1 = 0;
        z2 = 0;
        point = 0;
        Debug.Log("start passé");

    }

    // Update is called once per frame
    void Update()
    {

        if (Controller.GetHairTriggerDown())
        {
            if (point == 0)
            {
                Debug.Log("premier point pris");
                x1 = ControllerCube.transform.position.x;
                z1 = ControllerCube.transform.position.z;
                point++;
            }
            else if (point == 1)
            {
                Debug.Log("deuxième point pris");
                x2 = ControllerCube.transform.position.x;
                z2 = ControllerCube.transform.position.z;
                point++; //pour sortir du if (point==1) et donc éviter que la calibration soit faite en boucle
                float point1_x = firstCube.transform.position.x;
                float point1_z = firstCube.transform.position.z;
                float point2_x = secondCube.transform.position.x;
                float point2_z = secondCube.transform.position.z;
                Debug.Log("x1 reel=" + x1);
                Debug.Log("z1_reel=" + z1);
                Debug.Log("x2_reel=" + x2);
                Debug.Log("z2_reel=" + z2);
                Debug.Log("x1_fictif=" + point1_x);
                Debug.Log("z1_fictif=" + point1_z);
                Debug.Log("x2_fictif=" + point2_x);
                Debug.Log("z2_fictif=" + point2_z);
                float distance_reelle = Mathf.Sqrt(Mathf.Pow(x2 - x1, 2) + Mathf.Pow(z2 - z1, 2));
                float distance_virtuelle = Mathf.Sqrt(Mathf.Pow(point2_x - point1_x, 2) + Mathf.Pow(point2_z - point1_z, 2));
                //homotéthie
                float rapport = (distance_reelle * guitoune.transform.localScale.x) / distance_virtuelle;
                guitoune.transform.localScale = new Vector3(rapport, rapport, rapport);
                //translation

                point1_x = firstCube.transform.position.x;
                point1_z = firstCube.transform.position.z;
                point2_x = secondCube.transform.position.x;
                point2_z = secondCube.transform.position.z;

                //il est possible qu'il faille repointer ou récupérer à nouveau les coordonnées virtuelles et/ou réelles
                float translation_x = x1 - point1_x;
                float translation_z = z1 - point1_z;
                guitoune.transform.Translate(translation_x, 0, translation_z);

                Debug.Log("x1_fictif=" + point1_x);
                Debug.Log("z1_fictif=" + point1_z);
                Debug.Log("x2_fictif=" + point2_x);
                Debug.Log("z2_fictif=" + point2_z);
                //rotation

                point1_x = firstCube.transform.position.x;
                point1_z = firstCube.transform.position.z;


                point2_x = secondCube.transform.position.x;
                point2_z = secondCube.transform.position.z;

                /*float delta_z2 = z2 - z1;
                float delta_x2 = x2-point2_x;
                float theta_prime = Mathf.Atan(point2_x-point1_x / point2_z - point1_z) * 180 / (3.14159265f); //angle pour la distorsion entre la guitoune virtuelle et le repère de Unity
                float theta = Mathf.Atan((delta_x2 / delta_z2)* (point2_x - point1_x) / (point2_z - point1_z)) *180/(3.14159265f);*/
                //float theta = Mathf.Asin(delta_x2 /distance_reelle)*180/3.14159265f;
                //guitoune.transform.RotateAround(new Vector3(x1,0,z1), new Vector3(0, 1, 0), theta);
                //float delta_x = point2_x - x2;
                float theta_prime = Mathf.Atan((x1 - point2_x) / (point2_z - z1));//exprimé en radians
                float delta_x = (point2_x - x2) * Mathf.Cos(theta_prime) + (point2_z - z2) * Mathf.Sin(theta_prime);
                float delta_z = (x1 - x2) * Mathf.Sin(theta_prime) + (z2 - z1) * Mathf.Cos(theta_prime);
                float theta = -Mathf.Atan(delta_x / delta_z) * 180 / (3.14159265f);
                guitoune.transform.RotateAround(new Vector3(x1, 0, z1), new Vector3(0, 1, 0), theta);

                float distance = Mathf.Sqrt(Mathf.Pow(x2 - point2_x, 2) + Mathf.Pow(z2 - point2_z, 2));

                if (distance > 1.5 * distance_reelle)
                {
                    guitoune.transform.RotateAround(new Vector3(x1, 0, z1), new Vector3(0, 1, 0), 180);
                }

                Debug.Log("x1_fictif=" + firstCube.transform.position.x);
                Debug.Log("z1_fictif=" + firstCube.transform.position.z);
                Debug.Log("x2_fictif=" + secondCube.transform.position.x);
                Debug.Log("z2_fictif=" + secondCube.transform.position.z);
                //c'est fini

            }
        }

    }

    public void Calibrate()
    {
        point = 0;
    }
}
